"use strict";

// task 1

const button = document.getElementById('myButton');
const div = document.getElementById('myDiv');

button.addEventListener('click', () => {
    setTimeout(() => {
        div.textContent = 'Операція виконана успішно!';
        }, 3000);
});

// task 2 

const countdownDiv = document.getElementById('countdown');
let count = 10;

const countdownInterval = setInterval(() => {
    count--;
    countdownDiv.textContent = count;

    if (count === 0) {
        clearInterval(countdownInterval);
        countdownDiv.textContent = 'Зворотній відлік завершено';
        }
}, 1000);